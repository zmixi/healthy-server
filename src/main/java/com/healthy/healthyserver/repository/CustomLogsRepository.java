package com.healthy.healthyserver.repository;

import com.healthy.healthyserver.Entity.CustomLogs;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "custom_logs", path = "custom_logs")
public interface CustomLogsRepository extends PagingAndSortingRepository<CustomLogs, Long> {
    List<CustomLogs> findByDateAndUserid(@Param("date") String date, @Param("user_id") Long user_id);
}
