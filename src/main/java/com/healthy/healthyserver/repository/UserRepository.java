package com.healthy.healthyserver.repository;

import com.healthy.healthyserver.Entity.Custom;
import com.healthy.healthyserver.Entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "user", path = "user")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    List<User> findByWechatOpenId(@Param("openid") String openid);
}
