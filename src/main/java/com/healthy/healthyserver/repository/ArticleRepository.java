package com.healthy.healthyserver.repository;

import com.healthy.healthyserver.Entity.Article;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "article", path = "article")
public interface ArticleRepository extends PagingAndSortingRepository<Article, Long> {

    List<Article> findByTitle(@Param("title") String title);

}
