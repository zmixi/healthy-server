package com.healthy.healthyserver.repository;

import java.util.List;

import com.healthy.healthyserver.Entity.Custom;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "custom", path = "custom")
public interface CustomRepository extends PagingAndSortingRepository<Custom, Long> {

    List<Custom> findByAndUserid(@Param("user_id") Long user_id);

}
