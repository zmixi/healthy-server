package com.healthy.healthyserver.repository;

import com.healthy.healthyserver.Entity.WeightLogs;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "weight_logs", path = "weight_logs")
public interface WeightLogsRepository extends PagingAndSortingRepository<WeightLogs, Long> {
    List<WeightLogs> findByUserid(@Param("user_id") Long user_id);
}
