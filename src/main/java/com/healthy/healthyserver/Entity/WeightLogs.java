package com.healthy.healthyserver.Entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class WeightLogs {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String date;
    private Integer weight;
    private Long userid;

    protected WeightLogs() {}

    public WeightLogs(String date, Integer weight, Long user_id) {
        this.date = date;
        this.weight = weight;
        this.userid = user_id;
    }

    @Override
    public String toString() {
        return String.format(
                "Custom[id=%d, name='%s']",
                id, weight);
    }

}
