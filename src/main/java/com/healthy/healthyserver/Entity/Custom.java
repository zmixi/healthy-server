package com.healthy.healthyserver.Entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Custom {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;
    private Long userid;

    protected Custom() {}

    public Custom(String name, Long user_id) {
        this.name = name;
        this.userid = user_id;
    }

    @Override
    public String toString() {
        return String.format(
                "Custom[id=%d, name='%s']",
                id, name);
    }

}
