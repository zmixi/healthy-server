package com.healthy.healthyserver.Entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class CustomLogs {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String date;
    private String custom;
    private Long userid;
    private boolean value;

    protected CustomLogs() {}

    public CustomLogs(String date, String custom, Long user_id) {
        this.date = date;
        this.custom = custom;
        this.userid = user_id;
    }

    @Override
    public String toString() {
        return String.format(
                "Custom[id=%d, date='%s']",
                id, date);
    }

}
