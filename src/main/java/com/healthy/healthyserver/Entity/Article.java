package com.healthy.healthyserver.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Article {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String title;
    private String image;
    @Column(length = 16777216)
    private String content;

    protected Article() {}

    public Article(String title, String image, String content) {
        this.title = title;
        this.image = image;
        this.content = content;
    }

    @Override
    public String toString() {
        return String.format(
                "Article[id=%d, title='%s']",
                id, title);
    }

}
