package com.healthy.healthyserver.Entity;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String wechatOpenId;
    private String name;
    private String avatar;
    private Integer age;
    private String sex;
    private Integer height;
    private Integer beginWeight;
    private String targetWeight;
    private String healthyTarget;
    private String slogan;

    public User() {

    }

    public User(String name, String avatar,
             Integer age,
             String sex,
             Integer height,
             Integer beginWeight,
             String targetWeight,
             String healthyTarget,
             String slogan ) {
        this.name = name;
        this.avatar = avatar;
        this.age = age;
        this.sex = sex;
        this.height = height;
        this.beginWeight = beginWeight;
        this.targetWeight = targetWeight;
        this.healthyTarget = healthyTarget;
        this.slogan = slogan;
    }

    @Override
    public String toString() {
        return String.format(
                "Custom[id=%d, name='%s']",
                id, name);
    }

}
