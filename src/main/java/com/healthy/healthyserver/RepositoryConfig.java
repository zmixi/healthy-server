package com.healthy.healthyserver;

import com.healthy.healthyserver.Entity.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Article.class);
        config.exposeIdsFor(Custom.class);
        config.exposeIdsFor(CustomLogs.class);
        config.exposeIdsFor(User.class);
        config.exposeIdsFor(WeightLogs.class);
    }
}