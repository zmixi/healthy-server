package com.healthy.healthyserver.Controller.Message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Code2session {
    private Long errcode;
    private String errmsg;
    private String openid;
    private String session_key;
    private String unionid;
}
