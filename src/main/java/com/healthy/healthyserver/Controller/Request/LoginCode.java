package com.healthy.healthyserver.Controller.Request;

import lombok.Data;

@Data
public class LoginCode {
    private String code;
}
