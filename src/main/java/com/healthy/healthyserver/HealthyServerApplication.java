package com.healthy.healthyserver;

import com.healthy.healthyserver.Entity.Article;
import com.healthy.healthyserver.Entity.Custom;
import com.healthy.healthyserver.repository.CustomRepository;
import com.healthy.healthyserver.repository.ArticleRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;



@SpringBootApplication
public class HealthyServerApplication {

    private static final Logger log = LoggerFactory.getLogger(HealthyServerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(HealthyServerApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(ArticleRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Article("零热量又饱腹的第七类营养素", "../../images/news_1.png", "根据是否溶解于水，可将膳食纤维分为两大类：\n" +
                    "膳食纤维=可溶性膳食纤维+不可溶性膳食纤维，“可溶、不可溶，作用各不同”。\n" +
                    "1.可溶性膳食纤维\n" +
                    "来源于果胶、藻胶、魔芋等。魔芋盛产于我国四川等地，主要成分为葡甘聚糖，是一种可溶性膳食纤维，能量很低，吸水性强。很多研究表明，魔芋有降血脂和降血糖的作用及良好的通便作用；可溶性纤维在胃肠道内和淀粉等碳水化合物交织在一起，并延缓后者的吸收，故可以起到降低餐后血糖的作用；\n" +
                    "2.不可溶性膳食纤维\n" +
                    "最佳来源是全谷类粮食，其中包括麦麸、麦片、全麦粉及糙米、燕麦全谷类食物、豆类、蔬菜和水果等。不可溶性纤维对人体的作用首先在于促进胃肠道蠕动，加快食物通过胃肠道，减少吸收，另外不可溶性纤维在大肠中吸收水分软化大便，可以起到防治便秘的作用。\n" +
                    "3.膳食纤维\n" +
                    "如果将上述两者结合起来，膳食纤维的作用可列出长长的一串：\n" +
                    "（1）抗腹泻作用，如树胶和果胶等；\n" +
                    "（2）预防某些癌症，如肠癌等；\n" +
                    "（3）治疗便秘；\n" +
                    "（4）解毒；\n" +
                    "（5）预防和治疗肠道憩室病；\n" +
                    "（6）治疗胆石症；\n" +
                    "（7）降低血液胆固醇和甘油三酯；\n" +
                    "（8）控制体重等；\n" +
                    "（9）降低成年糖尿病患者的血糖。"));
            repository.save(new Article("大姨妈期间应该怎么吃？", "../../images/news_2.png", "根据是否溶解于水，可将膳食纤维分为两大类：\n" +
                    "膳食纤维=可溶性膳食纤维+不可溶性膳食纤维，“可溶、不可溶，作用各不同”。\n" +
                    "1.可溶性膳食纤维\n" +
                    "来源于果胶、藻胶、魔芋等。魔芋盛产于我国四川等地，主要成分为葡甘聚糖，是一种可溶性膳食纤维，能量很低，吸水性强。很多研究表明，魔芋有降血脂和降血糖的作用及良好的通便作用；可溶性纤维在胃肠道内和淀粉等碳水化合物交织在一起，并延缓后者的吸收，故可以起到降低餐后血糖的作用；\n" +
                    "2.不可溶性膳食纤维\n" +
                    "最佳来源是全谷类粮食，其中包括麦麸、麦片、全麦粉及糙米、燕麦全谷类食物、豆类、蔬菜和水果等。不可溶性纤维对人体的作用首先在于促进胃肠道蠕动，加快食物通过胃肠道，减少吸收，另外不可溶性纤维在大肠中吸收水分软化大便，可以起到防治便秘的作用。\n" +
                    "3.膳食纤维\n" +
                    "如果将上述两者结合起来，膳食纤维的作用可列出长长的一串：\n" +
                    "（1）抗腹泻作用，如树胶和果胶等；\n" +
                    "（2）预防某些癌症，如肠癌等；\n" +
                    "（3）治疗便秘；\n" +
                    "（4）解毒；\n" +
                    "（5）预防和治疗肠道憩室病；\n" +
                    "（6）治疗胆石症；\n" +
                    "（7）降低血液胆固醇和甘油三酯；\n" +
                    "（8）控制体重等；\n" +
                    "（9）降低成年糖尿病患者的血糖。"));
            repository.save(new Article("节食减肥的危害你知道吗？", "../../images/news_3.png", "根据是否溶解于水，可将膳食纤维分为两大类：\n" +
                    "膳食纤维=可溶性膳食纤维+不可溶性膳食纤维，“可溶、不可溶，作用各不同”。\n" +
                    "1.可溶性膳食纤维\n" +
                    "来源于果胶、藻胶、魔芋等。魔芋盛产于我国四川等地，主要成分为葡甘聚糖，是一种可溶性膳食纤维，能量很低，吸水性强。很多研究表明，魔芋有降血脂和降血糖的作用及良好的通便作用；可溶性纤维在胃肠道内和淀粉等碳水化合物交织在一起，并延缓后者的吸收，故可以起到降低餐后血糖的作用；\n" +
                    "2.不可溶性膳食纤维\n" +
                    "最佳来源是全谷类粮食，其中包括麦麸、麦片、全麦粉及糙米、燕麦全谷类食物、豆类、蔬菜和水果等。不可溶性纤维对人体的作用首先在于促进胃肠道蠕动，加快食物通过胃肠道，减少吸收，另外不可溶性纤维在大肠中吸收水分软化大便，可以起到防治便秘的作用。\n" +
                    "3.膳食纤维\n" +
                    "如果将上述两者结合起来，膳食纤维的作用可列出长长的一串：\n" +
                    "（1）抗腹泻作用，如树胶和果胶等；\n" +
                    "（2）预防某些癌症，如肠癌等；\n" +
                    "（3）治疗便秘；\n" +
                    "（4）解毒；\n" +
                    "（5）预防和治疗肠道憩室病；\n" +
                    "（6）治疗胆石症；\n" +
                    "（7）降低血液胆固醇和甘油三酯；\n" +
                    "（8）控制体重等；\n" +
                    "（9）降低成年糖尿病患者的血糖。"));
        };
    }
/*
    @Bean
    public CommandLineRunner demo(CustomRepository repository) {
        return (args) -> {
            // save a couple of customers
            repository.save(new Custom("Bauer"));
            repository.save(new Custom("Chloe"));
            repository.save(new Custom("Kim"));
            repository.save(new Custom("David"));
            repository.save(new Custom("Michelle"));



            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Custom customer : repository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            repository.findById(1L)
                    .ifPresent(customer -> {
                        log.info("Customer found with findById(1L):");
                        log.info("--------------------------------");
                        log.info(customer.toString());
                        log.info("");
                    });

            // fetch customers by last name
            log.info("Customer found with findByLastName('Bauer'):");
            log.info("--------------------------------------------");
            repository.findByName("Bauer").forEach(bauer -> {
                log.info(bauer.toString());
            });
            // for (Customer bauer : repository.findByLastName("Bauer")) {
            // 	log.info(bauer.toString());
            // }
            log.info("");
        };
    }*/
}

